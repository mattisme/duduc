#ifndef DUDU_H
#define DUDU_H

enum DuduStatus {
  DUDU_OK = 0,
  DUDU_WINDOW_ERR,
  DUDU_WINDOW_REQUEST_CLOSE,
  DUDU_INPUT_ERR,
  DUDU_GRAPHICS_ERR,
  DUDU_SCENE_ERR,
  DUDU_SCENE_REQUEST_CLOSE,
};

typedef void(*DuduBuilderCb);

typedef struct DuduBuilder {
  int window_width, window_height;
  char *window_title;
  int swap_interval;
  DuduBuilderCb step, draw;
} DuduBuilder;

int dudu_init(DuduBuilder *builder);
int dudu_step();

typedef unsigned int DuduEntity;
typedef int DuduType;

DuduEntity _scene_new_entity(const char *id_string, const char *type_string);
DuduEntity _scene_new_entities(unsigned int count, const char *type_string);

#define DUDU_ENTITY(id, ...) _scene_new_entity(#id, #__VA_ARGS__)
#define DUDU_ENTITIES(count, ...) _scene_new_entities(count, #__VA_ARGS__)

typedef struct Color01 {
  union {
    struct {
      float r, g, b, a;
    };
    float arr[4];
  };
} Color01;

Color01 color01(float r, float g, float b, float a);
Color01 color01_255(unsigned char r, unsigned char g, unsigned char b,
                    unsigned char a);
Color01 color01_white();
Color01 color01_black();
Color01 color01_red();
Color01 color01_green();
Color01 color01_blue();
float *color01_addr(Color01 *c);

typedef struct Vec2 {
  union {
    struct {
      float x, y;
    };
    float arr[2];
  };
} Vec2;

Vec2 vec2(float x, float y);
Vec2 vec2_zero();
Vec2 vec2_one();
Vec2 vec2_up();
Vec2 vec2_down();
Vec2 vec2_left();
Vec2 vec2_right();
Vec2 vec2_add(Vec2 a, Vec2 b);
Vec2 vec2_add_f(Vec2 v, float f);
Vec2 vec2_subtract(Vec2 a, Vec2 b);
Vec2 vec2_subtract_f(Vec2 v, float f);
Vec2 vec2_multiply(Vec2 a, Vec2 b);
Vec2 vec2_multiply_f(Vec2 v, float f);
Vec2 vec2_divide(Vec2 a, Vec2 b);
Vec2 vec2_divide_f(Vec2 v, float f);
int vec2_equal(Vec2 a, Vec2 b);
Vec2 vec2_abs(Vec2 v);
Vec2 vec2_floored(Vec2 v);
Vec2 vec2_ceiled(Vec2 v);
Vec2 vec2_rounded(Vec2 v);
Vec2 vec2_max(Vec2 a, Vec2 b);
Vec2 vec2_min(Vec2 a, Vec2 b);
Vec2 vec2_clamped(Vec2 v, float lower, float upper);
Vec2 vec2_negative(Vec2 v);
float vec2_dot(Vec2 a, Vec2 b);
float vec2_length2(Vec2 v);
float vec2_length(Vec2 v);
float vec2_distance2(Vec2 a, Vec2 b);
float vec2_distance(Vec2 a, Vec2 b);
Vec2 vec2_normalized(Vec2 v);

typedef struct Shader Shader;

Shader shader_from_source(const char *vs_src, const char *fs_src);
Shader shader_from_files(const char *vs_path, const char *fs_path);
Shader shader_from_dir(const char *path);
void shader_use(Shader *shader);

typedef struct Texture {
  unsigned int id;
  int w, h;
} Texture;

typedef struct TextureRegion {
  Texture *texture;
  float x, y;
  int w, h;
} TextureRegion;

Texture *texture_from_bytes(const unsigned char *bytes, int w, int h);
Texture *texture_from_file(const char *path);
void texture_bind(Texture *texture, int idx);
void texture_unbind();

TextureRegion texture_region(Texture *texture, float x, float y, int w, int h);
void texture_region_set_region(float x, float y, int w, int h);

void draw_rect(Vec2 pos, Vec2 dimen, char z);
void draw_sprite(TextureRegion texture_region, Vec2 pos, Vec2 dimen, char z);
void set_color(float r, float g, float b, float a);

typedef struct Transform {
  Vec2 position, scale;
} Transform;

Transform transform(Vec2 pos, Vec2 scale);
void transform_set_position(Transform *transform, Vec2 pos);
void transform_set_scale(Transform *transform, Vec2 scale);
void transform_translate(Transform *transform, Vec2 v);
void transform_scale(Transform *transform, Vec2 v);

typedef struct Renderer {
  TextureRegion texture_region;
  int centered : 1;  // 0 = false, anything else is true.
  Vec2 offset;
} Renderer;

Renderer renderer(TextureRegion texture_region, int centered, Vec2 offset);
void renderer_set_texture(Renderer *renderer, Texture *texture);
void renderer_set_region(Renderer *renderer, float x, float y, int w, int h);
void renderer_set_offset(Renderer *renderer, Vec2 offset);
void renderer_set_centered(Renderer *renderer, int centered);

typedef struct Animator {
} Animator;

#endif
