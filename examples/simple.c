#include <dudu.h>
#include <stdio.h>

int main() {
  int status;

  DuduBuilder builder;
  builder.window_width = 640;
  builder.window_height = 360;
  builder.window_title = "duduw_simple";

  if ((status = dudu_init(&builder)) != DUDU_OK) {
    fprintf(stderr, "Failed to initialize duduw. Error code: %i\n", status);
    return status;
  }

  DUDU_ENTITY(MyEntity, Sprite);

  while ((status = dudu_step()) == DUDU_OK) {
    // Anything to do at the very end of each frame.
  }
  // Anything to do at the very end of the program.
}
