#include <epoxy/gl.h>
#include <stdio.h>
#include <stdlib.h>
#include "graphics.h"

static void assert_shader_compile_success(unsigned int id, int shader) {
  int success;
  int len = 0;
  char *msg = NULL;

  if (shader == 0) {
    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE) {
      glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
      if (len > 0) {
        msg = malloc(len);
        glGetShaderInfoLog(id, len, NULL, msg);
      }
    }
  } else {
    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if (success == GL_FALSE) {
      glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);
      if (len > 0) {
        msg = malloc(len);
        glGetProgramInfoLog(id, len, NULL, msg);
      }
    }
  }
  if (success == GL_FALSE) {
    if (msg[0] != '\0') {
      fprintf(stderr, "OpenGL shader error: %s\n", msg);
    } else {
      fprintf(stderr,
              "OpenGL shader error: Unable to retrieve error message.\n");
    }
    free(msg);
    abort();
  }
}

static unsigned int compile_shader(GLenum type, const char *src) {
  unsigned int result = glCreateShader(type);
  glShaderSource(result, 1, &src, NULL);
  glCompileShader(result);

  assert_shader_compile_success(result, 0);
  return result;
}

static void set_locations(Shader *shader) {
  shader->locations[LOC_POS] = glGetAttribLocation(shader->id, "vPosition");
  shader->locations[LOC_REGION] = glGetAttribLocation(shader->id, "vRegion");
  shader->locations[LOC_COLOR] = glGetAttribLocation(shader->id, "vColor");
  shader->locations[LOC_DIFF] = glGetUniformLocation(shader->id, "diffuse");
  shader->locations[LOC_TEX] = glGetUniformLocation(shader->id, "tex");
  shader->locations[LOC_VIEW] = glGetUniformLocation(shader->id, "view");

  for (int i = 0; i < SHADER_LOCATION_LEN; i++) {
    printf("%i\n", shader->locations[i]);
  }
}

Shader shader_from_source(const char *vs_src, const char *fs_src) {
  Shader shader;
  shader.id = glCreateProgram();

  int vs = compile_shader(GL_VERTEX_SHADER, vs_src);
  int fs = compile_shader(GL_FRAGMENT_SHADER, fs_src);

  glAttachShader(shader.id, vs);
  glAttachShader(shader.id, fs);

  glLinkProgram(shader.id);
  glValidateProgram(shader.id);

  assert_shader_compile_success(shader.id, 1);

  glDeleteShader(vs);
  glDeleteShader(fs);

  shader_use(&shader);
  set_locations(&shader);
  return shader;
}

void shader_use(Shader *shader) { glUseProgram(shader->id); }
