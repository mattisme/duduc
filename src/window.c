#include "window.h"
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>

GLFWwindow *window = NULL;

static void glfw_error_cb(int err, const char *msg) {
  fprintf(stderr, "GLFW error %i: %s\n", err, msg);
  abort();
}

int window_init(int w, int h, char *title, int swap_interval) {
  if (!glfwInit()) {
    fprintf(stderr, "Failed to initialize GLFW.\n");
    return -1;
  }

  glfwSetErrorCallback(glfw_error_cb);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
  glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);

  window = glfwCreateWindow(w, h, title, NULL, NULL);
  if (!window) {
    fprintf(stderr, "Failed to create window.\n");
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  return 0;
}

inline void *_window_get() { return window; }

int window_update_or_close() {
  glfwSwapBuffers(window);
  glfwPollEvents();
  return glfwWindowShouldClose(window);
}

inline double window_get_time() { return glfwGetTime(); }
