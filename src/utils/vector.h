#ifndef DUDU_VECTOR_H
#define DUDU_VECTOR_H

#include <stdlib.h>

#define DEFAULT_INIT_CAPACITY 4

#define VECTOR_INIT_WITH_CAPACITY(vec, capacity) vector vec; vector_init_with_capacity(&vec, capacity)
#define VECTOR_INIT(vec) vector vec; vector_init(&vec)
#define VECTOR_LEN(vec) vector_len(&vec)
#define VECTOR_PUSH(vec, item) vector_push(&vec, (void *) item)
#define VECTOR_SET(vec, idx, item) vector_set(&vec, idx, (void *) item)
#define VECTOR_GET(vec, type, idx) (type) vector_get(&vec, idx)
#define VECTOR_DELETE(vec, idx) vector_delete(&vec, idx)
#define VECTOR_FREE(vec) vector_free(&vec)

typedef struct Vector {
  void **items;
  size_t capacity;
  size_t size;
} Vector;

void vector_init_with_capacity(Vector *, size_t capacity);
void vector_init(Vector *);
size_t vector_len(Vector *);
void vector_push(Vector *, void *);
void vector_set(Vector *, size_t, void *);
void *vector_get(Vector *, size_t);
void vector_delete(Vector *, size_t);
void vector_free(Vector *);

#endif
