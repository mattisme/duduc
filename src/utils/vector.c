#include "vector.h"
#include <stdlib.h>

void vector_init_with_capacity(Vector *v, size_t capacity) {
  v->capacity = capacity;
  v->size = 0;
  v->items = malloc(sizeof(void *) * v->capacity);
}

void vector_init(Vector *v) {
  return vector_init_with_capacity(v, DEFAULT_INIT_CAPACITY);
}

size_t vector_len(Vector *v) { return v->size; }

void vector_resize(Vector *v, size_t capacity) {
  void **items = realloc(v->items, sizeof(void *) * capacity);
  if (items) {
    v->items = items;
    v->capacity = capacity;
  }
}

void vector_push(Vector *v, void *item) {
  if (v->capacity == v->size) {
    vector_resize(v, v->capacity * 2);
  }
  v->items[v->size++] = item;
}

void vector_set(Vector *v, size_t idx, void *item) {
  if (idx < v->size) {
    v->items[idx] = item;
  }
}

void *vector_get(Vector *v, size_t idx) {
  if (idx < v->size) {
    return v->items[idx];
  }
  return NULL;
}

void vector_delete(Vector *v, size_t idx) {
  if (idx >= v->size) {
    return;
  }

  v->items[idx] = NULL;

  for (int i = idx; i < v->size - 1; i++) {
    v->items[i] = v->items[i + 1];
    v->items[i + 1] = NULL;
  }

  v->size--;

  if (v->size > 0 && v->size == v->capacity / 4) {
    vector_resize(v, v->capacity / 2);
  }
}

void vector_free(Vector *v) { free(v->items); }
