#include <epoxy/gl.h>
#include <stdlib.h>
#include "graphics.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"

inline TextureRegion texture_region(Texture *texture, float x, float y, int w,
                                    int h) {
  TextureRegion result;
  result.texture = texture;
  result.x = x;
  result.y = y;
  result.w = w;
  result.h = h;
  return result;
}

Texture *texture_from_bytes(const unsigned char *bytes, int w, int h) {
  Texture *texture = malloc(sizeof(Texture));
  if (!texture) {
    fprintf(stderr,
            "texture_from_bytes() Failed to allocate memory for texture.\n");
    return NULL;
  }
  glGenTextures(1, &texture->id);
  glBindTexture(GL_TEXTURE_2D, texture->id);

  texture->w = w;
  texture->h = h;

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE,
               bytes);
  glBindTexture(GL_TEXTURE_2D, 0);

  return texture;
}

Texture *texture_from_file(const char *path) {
  int w, h, channels;
  unsigned char *bytes = stbi_load(path, &w, &h, &channels, 0);
  if (!bytes) {
    fprintf(stderr, "texture_from_file() Unable to load image.");
    abort();
  }

  return texture_from_bytes(bytes, w, h);
}

void texture_bind(Texture *texture, int idx) {
  glActiveTexture(GL_TEXTURE0 + idx);
  glBindTexture(GL_TEXTURE_2D, texture->id);
}

void texture_unbind() { glBindTexture(GL_TEXTURE_2D, 0); }
