#include <GLFW/glfw3.h>
#include <dudu.h>
#include <stdio.h>
#include <stdlib.h>
#include "window.h"

enum StateType {
  STATE_TYPE_PRESSED,
  STATE_TYPE_RELEASED,
};

struct state {
  unsigned int last, current : 1;
};

typedef struct InputState {
  struct state keys[GLFW_KEY_LAST];
  struct state mouse_btns[GLFW_MOUSE_BUTTON_LAST];
  Vec2 mouse_position;
} InputState;

InputState input_state;

static inline void debug_print_input(int input, enum StateType type) {
  if (type == STATE_TYPE_PRESSED) {
    printf("Pressed: %i\n", input);
  } else {
    printf("Released: %i\n", input);
  }
}

static inline void key_on_press(int key) {
  debug_print_input(key, STATE_TYPE_PRESSED);
  input_state.keys[key].current = STATE_TYPE_PRESSED;
}

static inline void key_on_release(int key) {
  debug_print_input(key, STATE_TYPE_RELEASED);
  input_state.keys[key].current = STATE_TYPE_RELEASED;
}

static inline void mouse_on_press(int btn) {
  debug_print_input(btn, STATE_TYPE_PRESSED);
  input_state.mouse_btns[btn].current = STATE_TYPE_PRESSED;
}

static inline void mouse_on_release(int btn) {
  debug_print_input(btn, STATE_TYPE_RELEASED);
  input_state.mouse_btns[btn].current = STATE_TYPE_RELEASED;
}

static void glfw_key_cb(GLFWwindow *window, int key, int scancode, int action,
                        int mods) {
  switch (action) {
    case GLFW_PRESS:
      key_on_press(key);
      break;
    case GLFW_RELEASE:
      key_on_release(key);
      break;
    default:
      fprintf(stderr, "glfw_key_cb() GLFW action type is not handled.\n");
      abort();
  }
}

static void glfw_mouse_cb(GLFWwindow *window, int btn, int action, int mods) {
  switch (action) {
    case GLFW_PRESS:
      mouse_on_press(btn);
      break;
    case GLFW_RELEASE:
      mouse_on_release(btn);
      break;
    default:
      fprintf(stderr, "glfw_mouse_cb() GLFW action type is not handled.\n");
      abort();
  }
}

static void glfw_cursor_pos_cb(GLFWwindow *window, double x, double y) {
  input_state.mouse_position.x = x;
  input_state.mouse_position.y = y;
  /*printf("Mouse pos: (%f, %f)\n", input_state.mouse_position[0],*/
  /*input_state.mouse_position[1]);*/
}

int input_init() {
  GLFWwindow *window = _window_get();
  if (!window) {
    fprintf(stderr, "_window_get() did not return a valid window.\n");
    return -1;
  }

  glfwSetKeyCallback(window, glfw_key_cb);
  glfwSetMouseButtonCallback(window, glfw_mouse_cb);
  glfwSetCursorPosCallback(window, glfw_cursor_pos_cb);
  return 0;
}

void input_update() { input_state.keys->last = input_state.keys->current; }
