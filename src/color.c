#include <dudu.h>

inline Color01 color01(float r, float g, float b, float a) {
  Color01 result;
  result.r = r;
  result.g = g;
  result.b = b;
  result.a = a;
  return result;
}

inline Color01 color01_255(unsigned char r, unsigned char g, unsigned char b,
                           unsigned char a) {
  return color01(r / 255, g / 255, b / 255, a / 255);
}

inline Color01 color01_white() { return color01(1, 1, 1, 1); }
inline Color01 color01_black() { return color01(0, 0, 0, 1); }
inline Color01 color01_red() { return color01(1, 0, 0, 1); }
inline Color01 color01_green() { return color01(0, 1, 0, 1); }
inline Color01 color01_blue() { return color01(0, 0, 1, 1); }
inline float *color01_addr(Color01 *c) { return (float *)c; }
