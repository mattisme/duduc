#include <dudu.h>
#include "../graphics.h"
#include "../scene.h"

void renderer_on_add(ecs_rows_t *rows) {
  ECS_COLUMN(rows, Renderer, renderer, 1);

  for (int i = 0; i < rows->count; i++) {
    renderer[i].texture_region = texture_region(NULL, 0, 0, 1, 1);
    renderer[i].offset = vec2_zero();
  }
}

void renderer_draw(ecs_rows_t *rows) {
  ECS_COLUMN(rows, Renderer, renderer, 1);
  ECS_COLUMN(rows, Transform, transform, 2);

  for (int i = 0; i < rows->count; i++) {
    Renderer *r = &renderer[i];
    Transform *t = &transform[i];

    // Ensure the texture.id is valid.
    if (r->texture_region.texture->id <= 0) continue;

    Vec2 dimen = vec2(r->texture_region.texture->w * t->scale.x,
                      r->texture_region.texture->h * t->scale.y);

    Vec2 pos = t->position;
    if (r->centered != 0) {
      pos = vec2_add(pos, vec2_divide_f(dimen, 2));
    }
    pos = vec2_add(pos, r->offset);

    draw_sprite(r[i].texture_region, pos, dimen, 0);
  }
}

inline Renderer renderer(TextureRegion texture_region, int centered,
                         Vec2 offset) {
  Renderer result;
  result.texture_region = texture_region;
  result.centered = centered;
  result.offset = offset;
  return result;
}

void renderer_set_texture(Renderer *renderer, Texture *texture) {
  renderer->texture_region.texture = texture;
}

void renderer_set_region(Renderer *renderer, float x, float y, int w, int h) {
  renderer->texture_region.x = x;
  renderer->texture_region.y = y;
  renderer->texture_region.w = w;
  renderer->texture_region.h = h;
}

void renderer_set_offset(Renderer *renderer, Vec2 offset) {
  renderer->offset = offset;
}

void renderer_set_centered(Renderer *renderer, int centered) {
  renderer->centered = centered;
}
