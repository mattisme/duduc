#include <flecs.h>
#include "../scene.h"
#include "dudu.h"

void transform_on_add(ecs_rows_t *rows) {
  ECS_COLUMN(rows, Transform, transform, 1);

  for (int i = 0; i < rows->count; i++) {
    transform[i].position = vec2_zero();
    transform[i].scale = vec2_one();
  }
}

inline Transform transform(Vec2 pos, Vec2 scale) {
  Transform result;
  result.position = pos;
  result.scale = scale;
  return result;
}

void transform_set_position(Transform *transform, Vec2 pos) {
  transform->position = pos;
}

void transform_set_scale(Transform *transform, Vec2 scale) {
  transform->scale = scale;
}

void transform_translate(Transform *transform, Vec2 v) {
  v = vec2_add(transform->position, v);
}

void transform_scale(Transform *transform, Vec2 v) {
  v = vec2_multiply(transform->scale, v);
}
