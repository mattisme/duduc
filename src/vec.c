#include <dudu.h>
#include <math.h>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))

enum VecOperationType {
  OP_ADD,
  OP_SUB,
  OP_MUL,
  OP_DIV,
};

static inline void clamp(float *f, float lower, float upper) {
  *f = MAX(lower, MIN(*f, upper));
}

static void vec_op(float *result, float *a, float *b, int n,
                   enum VecOperationType type) {
  for (int i = 0; i < n; i++) {
    switch (type) {
      case OP_ADD:
        result[i] = a[i] + b[i];
        break;
      case OP_SUB:
        result[i] = a[i] - b[i];
        break;
      case OP_MUL:
        result[i] = a[i] * b[i];
        break;
      case OP_DIV:
        result[i] = a[i] / b[i];
        break;
    }
  }
}

static void vec_op_fn(float *result, double (*fn)(double v), int n) {
  for (int i = 0; i < n; i++) {
    fn(result[i]);
  }
}

static void vec_op_f(float *result, float *v, float f, int n,
                     enum VecOperationType type) {
  for (int i = 0; i < n; i++) {
    switch (type) {
      case OP_ADD:
        result[i] = v[i] + f;
        break;
      case OP_SUB:
        result[i] = v[i] - f;
        break;
      case OP_MUL:
        result[i] = v[i] * f;
        break;
      case OP_DIV:
        result[i] = v[i] / f;
        break;
    }
  }
}

inline Vec2 vec2(float x, float y) {
  Vec2 v;
  v.x = x;
  v.y = y;
  return v;
}

inline Vec2 vec2_zero() { return vec2(0, 0); }
inline Vec2 vec2_one() { return vec2(1, 1); }
inline Vec2 vec2_up() { return vec2(0, -1); }
inline Vec2 vec2_down() { return vec2(0, 1); }
inline Vec2 vec2_left() { return vec2(-1, 0); }
inline Vec2 vec2_right() { return vec2(1, 0); }

inline Vec2 vec2_add(Vec2 a, Vec2 b) {
  Vec2 result;
  vec_op((float *)&result, (float *)&a, (float *)&b, 2, OP_ADD);
  return result;
}

inline Vec2 vec2_add_f(Vec2 v, float f) {
  Vec2 result;
  vec_op_f((float *)&result, (float *)&v, f, 2, OP_ADD);
  return result;
}

inline Vec2 vec2_subtract(Vec2 a, Vec2 b) {
  Vec2 result;
  vec_op((float *)&result, (float *)&a, (float *)&b, 2, OP_SUB);
  return result;
}

inline Vec2 vec2_subtract_f(Vec2 v, float f) {
  Vec2 result;
  vec_op_f((float *)&result, (float *)&v, f, 2, OP_SUB);
  return result;
}

inline Vec2 vec2_multiply(Vec2 a, Vec2 b) {
  Vec2 result;
  vec_op((float *)&result, (float *)&a, (float *)&b, 2, OP_MUL);
  return result;
}

inline Vec2 vec2_multiply_f(Vec2 v, float f) {
  Vec2 result;
  vec_op_f((float *)&result, (float *)&v, f, 2, OP_MUL);
  return result;
}

inline Vec2 vec2_divide(Vec2 a, Vec2 b) {
  Vec2 result;
  vec_op((float *)&result, (float *)&a, (float *)&b, 2, OP_DIV);
  return result;
}

inline Vec2 vec2_divide_f(Vec2 v, float f) {
  Vec2 result;
  vec_op_f((float *)&result, (float *)&v, f, 2, OP_DIV);
  return result;
}

inline int vec2_equal(Vec2 a, Vec2 b) { return a.x == b.x && a.y == b.y; }

inline Vec2 vec2_abs(Vec2 v) {
  Vec2 result = v;
  vec_op_fn((float *)&result, fabs, 2);
  return result;
}
inline Vec2 vec2_floored(Vec2 v) {
  Vec2 result = v;
  vec_op_fn((float *)&result, floor, 2);
  return result;
}
inline Vec2 vec2_ceiled(Vec2 v) {
  Vec2 result = v;
  vec_op_fn((float *)&result, ceil, 2);
  return result;
}
inline Vec2 vec2_rounded(Vec2 v) {
  Vec2 result = v;
  vec_op_fn((float *)&result, round, 2);
  return result;
}
inline Vec2 vec2_max(Vec2 a, Vec2 b) {
  Vec2 result;
  result.x = MAX(a.x, b.x);
  result.y = MAX(a.y, b.y);
  return result;
}
inline Vec2 vec2_min(Vec2 a, Vec2 b) {
  Vec2 result;
  result.x = MIN(a.x, b.x);
  result.y = MIN(a.y, b.y);
  return result;
}
inline Vec2 vec2_clamped(Vec2 v, float lower, float upper) {
  Vec2 result = v;
  for (int i = 0; i < 2; i++) {
    clamp((float *)&result, lower, upper);
  }
  return result;
}
inline Vec2 vec2_negative(Vec2 v) { return vec2_multiply_f(v, -1.0); }
inline float vec2_dot(Vec2 a, Vec2 b) { return a.x * b.x + a.y * b.y; }
inline float vec2_length2(Vec2 v) { return (v.x * v.x) + (v.y * v.y); }
inline float vec2_length(Vec2 v) { return sqrtf(vec2_length2(v)); }
inline float vec2_distance2(Vec2 a, Vec2 b) {
  return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
}
inline float vec2_distance(Vec2 a, Vec2 b) {
  return sqrt(vec2_distance2(a, b));
}
inline Vec2 vec2_normalized(Vec2 v) {
  Vec2 result;
  vec_op_f((float *)&result, (float *)&v, vec2_length(v), 2, OP_DIV);
  return result;
}
