#include <dudu.h>
#include <stdio.h>
#include "graphics.h"
#include "scene.h"
#include "window.h"

static const float desired_framerate = 1.0f / 60.0f;
static const int max_updates = 5;

double last_time, delta_time, accumulation, timer = 0.0;
int frames, updates = 0;

TextureRegion test;

int dudu_init(DuduBuilder *builder) {
  // Initialize.
  printf("Initializing dudu.\n");

  int w =
      builder->window_width != 0 ? builder->window_width : DEFAULT_WINDOW_WIDTH;
  int h = builder->window_height != 0 ? builder->window_height
                                      : DEFAULT_WINDOW_HEIGHT;
  char *title = builder->window_title[0] != '\0' ? builder->window_title
                                                 : DEFAULT_WINDOW_TITLE;

  printf("w: %i, h %i\n", w, h);

  if (window_init(w, h, title, builder->swap_interval) != 0) {
    fprintf(stderr, "Failed to initialize window.\n");
    return DUDU_WINDOW_ERR;
  };

  if (input_init() != 0) {
    fprintf(stderr, "Failed to initialize input.\n");
    return DUDU_INPUT_ERR;
  }

  if (graphics_init(w, h) != 0) {
    fprintf(stderr, "Failed to initialize graphics.\n");
    return DUDU_GRAPHICS_ERR;
  }

  if (scene_init() != 0) {
    fprintf(stderr, "Failed to initialize scene.\n");
    return DUDU_SCENE_ERR;
  }

  test = texture_region(texture_from_file("/home/matt/Pictures/test.png"), 0, 0,
                        1, 1);

  last_time = window_get_time();
  timer = last_time;
  return 0;
}

int dudu_step() {
  if (window_update_or_close()) {
    /*printf("close.\n");*/
    return DUDU_WINDOW_REQUEST_CLOSE;
  }

  double current_time = window_get_time();
  delta_time = current_time - last_time;
  accumulation += delta_time / desired_framerate;
  last_time = current_time;

  updates = 0;
  while (accumulation >= 1.0) {
    if (updates == max_updates) {
      accumulation = 0.0;
      break;
    }

    if (!scene_update(delta_time)) {
      return DUDU_SCENE_REQUEST_CLOSE;
    }

    updates++;
    accumulation--;
  }

  scene_draw();

  // Flush to display.
  graphics_flush();

  frames++;
  if (window_get_time() - timer > 1.0) {
    timer++;

    printf("FPS: %i, DT: %f\n", frames, delta_time);
    frames = 0;
    delta_time = 0;
  }
  return 0;
}
