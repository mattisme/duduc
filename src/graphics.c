#include "graphics.h"
#include <epoxy/gl.h>
#include "utils/vector.h"

#include <stdio.h>
#include <stdlib.h>

#define MAX_CALLS_PER_BATCH 8192

const char *DEFAULT_VS =
    "#version 330\n"
    "in vec2 vPosition;\n"
    "in vec2 vRegion;\n"
    "in vec4 vColor;\n"
    "out vec2 fRegion;\n"
    "out vec4 fColor;\n"
    "uniform mat4 view;\n"
    "void main()\n"
    "{\n"
    "  fRegion = vRegion;\n"
    "  fColor = vColor;\n"
    "  gl_Position = view * vec4(vPosition, 1.0, 1.0);\n"
    "}\n";

const char *DEFAULT_FS =
    "#version 330\n"
    "in vec2 fRegion;\n"
    "in vec4 fColor;\n"
    "uniform vec4 diffuse;\n"
    "uniform sampler2D tex;\n"
    "out vec4 finalColor;\n"
    "void main()\n"
    "{\n"
    "  finalColor = texture(tex, fRegion) * fColor * diffuse;\n"
    "}\n";

struct BatchConfig {
  int mode;
  Texture *texture;
  char z;
};

struct Batch {
  float *pos, *region, *color;
  unsigned int *indices;
  int p_off, r_off, c_off;
  unsigned int vao;
  unsigned int vbo[4];
  int count, empty_passes;
  struct BatchConfig config;
};

struct Graphics {
  Vector batches;
  Texture *default_texture, *current_texture;
  Shader default_shader, current_shader;
  struct Mat4 current_perspective;
  Color01 current_diffuse, current_color, current_background_color;
};

static struct Graphics graphics;

void assert_gl_no_err() {
  GLenum err;
  while ((err = glGetError()) != GL_NO_ERROR) {
    fprintf(stderr, "OpenGL error: %i\n", err);
    abort();
  }
}

static struct Batch *batch_init(struct BatchConfig config) {
  struct Batch *batch = malloc(sizeof(struct Batch));
  batch->config = config;
  batch->pos = calloc(8 * MAX_CALLS_PER_BATCH, sizeof(float));
  batch->region = calloc(8 * MAX_CALLS_PER_BATCH, sizeof(float));
  batch->color = calloc(16 * MAX_CALLS_PER_BATCH, sizeof(float));
  batch->indices = malloc(sizeof(unsigned int) * 6 * MAX_CALLS_PER_BATCH);

  glGenVertexArrays(1, &batch->vao);
  glBindVertexArray(batch->vao);

  glGenBuffers(1, &batch->vbo[0]);
  glBindBuffer(GL_ARRAY_BUFFER, batch->vbo[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8 * MAX_CALLS_PER_BATCH,
               batch->pos, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray(graphics.current_shader.locations[LOC_POS]);
  glVertexAttribPointer(graphics.current_shader.locations[LOC_POS], 2, GL_FLOAT,
                        false, 0, NULL);

  glGenBuffers(1, &batch->vbo[1]);
  glBindBuffer(GL_ARRAY_BUFFER, batch->vbo[1]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8 * MAX_CALLS_PER_BATCH,
               batch->region, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray(graphics.current_shader.locations[LOC_REGION]);
  glVertexAttribPointer(graphics.current_shader.locations[LOC_REGION], 2,
                        GL_FLOAT, false, 0, NULL);

  glGenBuffers(1, &batch->vbo[2]);
  glBindBuffer(GL_ARRAY_BUFFER, batch->vbo[2]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 16 * MAX_CALLS_PER_BATCH,
               batch->color, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray(graphics.current_shader.locations[LOC_COLOR]);
  glVertexAttribPointer(graphics.current_shader.locations[LOC_COLOR], 4,
                        GL_FLOAT, false, 0, NULL);

  unsigned int j = 0;
  for (int i = 0; i < (6 * MAX_CALLS_PER_BATCH); i += 6, j += 4) {
    batch->indices[i] = j;
    batch->indices[i + 1] = j + 1;
    batch->indices[i + 2] = j + 2;
    batch->indices[i + 3] = j;
    batch->indices[i + 4] = j + 3;
    batch->indices[i + 5] = j + 1;
  }

  glGenBuffers(1, &batch->vbo[3]);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->vbo[3]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               sizeof(unsigned int) * 6 * MAX_CALLS_PER_BATCH, batch->indices,
               GL_STATIC_DRAW);

  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  batch->p_off = 0;
  batch->r_off = 0;
  batch->c_off = 0;
  batch->count = 0;
  batch->empty_passes = 0;

  VECTOR_PUSH(graphics.batches, batch);
  return batch;
}

static void free_batch(struct Batch *batch) {
  free(batch->pos);
  free(batch->region);
  free(batch->color);
  free(batch->indices);
  free(batch);
}

inline static void add_pos_vertices(struct Batch *batch, Vec2 pos, Vec2 dimen) {
  batch->pos[batch->p_off] = pos.x;
  batch->pos[batch->p_off + 1] = pos.y;
  batch->pos[batch->p_off + 2] = pos.x + dimen.x;
  batch->pos[batch->p_off + 3] = pos.y + dimen.y;
  batch->pos[batch->p_off + 4] = pos.x;
  batch->pos[batch->p_off + 5] = pos.y + dimen.y;
  batch->pos[batch->p_off + 6] = pos.x + dimen.x;
  batch->pos[batch->p_off + 7] = pos.y;
  batch->p_off += 8;
}

inline static void add_region_vertices(struct Batch *batch,
                                       TextureRegion texture_region) {
  batch->region[batch->r_off] = texture_region.x;
  batch->region[batch->r_off + 1] = texture_region.y;
  batch->region[batch->r_off + 2] = texture_region.x + texture_region.w;
  batch->region[batch->r_off + 3] = texture_region.y + texture_region.h;
  batch->region[batch->r_off + 4] = texture_region.x;
  batch->region[batch->r_off + 5] = texture_region.y + texture_region.h;
  batch->region[batch->r_off + 6] = texture_region.x + texture_region.w;
  batch->region[batch->r_off + 7] = texture_region.y;
  batch->r_off += 8;
}

inline static void add_color_vertices(struct Batch *batch, Color01 color) {
  for (int i = 0; i < 16; i += 4) {
    batch->color[batch->c_off + i] = color.r;
    batch->color[batch->c_off + i + 1] = color.g;
    batch->color[batch->c_off + i + 2] = color.b;
    batch->color[batch->c_off + i + 3] = color.a;
  }
  batch->c_off += 16;
}

inline static int has_space(struct Batch *batch) {
  return batch->count + 1 <= MAX_CALLS_PER_BATCH;
}

inline static int is_compatible(struct Batch *batch,
                                struct BatchConfig config) {
  return batch->config.mode == config.mode &&
         batch->config.texture->id == config.texture->id &&
         batch->config.z == config.z;
}

static void batch_flush(struct Batch *batch) {
  glBindVertexArray(batch->vao);

  glBindBuffer(GL_ARRAY_BUFFER, batch->vbo[0]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 8 * batch->count,
                  batch->pos);

  glBindBuffer(GL_ARRAY_BUFFER, batch->vbo[1]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 8 * batch->count,
                  batch->region);

  glBindBuffer(GL_ARRAY_BUFFER, batch->vbo[2]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 16 * batch->count,
                  batch->color);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->vbo[3]);
  glDrawElements(GL_TRIANGLES, 6 * batch->count, GL_UNSIGNED_INT, NULL);

  batch->p_off = 0;
  batch->r_off = 0;
  batch->c_off = 0;
  batch->count = 0;
  batch->empty_passes = 0;
}

inline static void assert_batch_not_null(struct Batch *batch, const char *msg) {
  if (!batch) {
    fprintf(stderr, "assert_batch_not_null()\n%s\n", msg);
    abort();
  }
}

static struct Batch *get_batch(struct BatchConfig config) {
  struct Batch *batch = NULL;
  struct Batch *empty = NULL;

  for (int i = 0; i < VECTOR_LEN(graphics.batches); i++) {
    batch = VECTOR_GET(graphics.batches, struct Batch *, i);

    if (has_space(batch) && is_compatible(batch, config)) {
      return batch;
    }

    if (!empty && batch->count == 0) {
      empty = batch;
    }
  }

  if (empty) {
    empty->config = config;
    return empty;
  }

  return batch_init(config);
}

int graphics_init(int w, int h) {
  Color01 bg_color = color01(0.05, 0.05, 0.05, 1.0);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glClearColor(bg_color.r, bg_color.g, bg_color.b, bg_color.a);

  printf("OpenGL version: %i\n", epoxy_gl_version());

  unsigned char default_texture[4];
  for (int i = 0; i < 4; i++) {
    default_texture[i] = 255;
  }
  vector_init(&graphics.batches);

  graphics.current_shader = shader_from_source(DEFAULT_VS, DEFAULT_FS);

  graphics.default_texture = texture_from_bytes(default_texture, 1, 1);
  glUniform1i(graphics.current_shader.locations[LOC_TEX], 0);

  graphics.current_perspective = mat4_ortho(0.0f, w, h, 0.0f, -1.0f, 1.0f);
  glUniformMatrix4fv(graphics.current_shader.locations[LOC_VIEW], 1, false,
                     mat4_addr(&graphics.current_perspective));

  graphics.current_diffuse = color01_white();
  glUniform4fv(graphics.current_shader.locations[LOC_DIFF], 1,
               color01_addr(&graphics.current_diffuse));

  graphics.current_color = color01_white(graphics.current_color);
  graphics.current_background_color = bg_color;

  assert_gl_no_err();
  return 0;
}

void graphics_flush() {
  assert_gl_no_err();
  glClear(GL_COLOR_BUFFER_BIT);

  int idx = -1;
  struct Batch *batch = NULL;

  for (int i = 0; i < VECTOR_LEN(graphics.batches); i++) {
    batch = VECTOR_GET(graphics.batches, struct Batch *, i);

    if (batch->count > 0) {
      if (!graphics.current_texture) {
        texture_bind(batch->config.texture, 0);
        graphics.current_texture = batch->config.texture;
      }
      if (batch->config.texture->id != graphics.current_texture->id) {
        texture_bind(batch->config.texture, 0);
        graphics.current_texture = batch->config.texture;
      }
      batch_flush(batch);
    } else {
      if (idx < 0 && batch->empty_passes > 60) {
        idx = i;
      } else {
        batch->empty_passes++;
      }
    }
  }

  if (idx >= 0) {
    free_batch(VECTOR_GET(graphics.batches, struct Batch *, idx));
    VECTOR_DELETE(graphics.batches, idx);
  }

  set_color(1, 1, 1, 1);
}

void draw_rect(Vec2 pos, Vec2 dimen, char z) {
  struct BatchConfig config;
  config.mode = GL_TRIANGLES;
  config.texture = graphics.default_texture;
  config.z = z;

  struct Batch *batch = get_batch(config);

  assert_batch_not_null(batch, "draw_rect(), Unable to get batch.");

  add_pos_vertices(batch, pos, dimen);
  add_color_vertices(batch, graphics.current_color);

  assert_gl_no_err();

  batch->count++;
}

void draw_sprite(TextureRegion texture_region, Vec2 pos, Vec2 dimen, char z) {
  struct BatchConfig config;
  config.mode = GL_TRIANGLES;
  config.texture = texture_region.texture;
  config.z = z;

  struct Batch *batch = get_batch(config);

  assert_batch_not_null(batch, "draw_sprite(), Unable to get batch.");

  add_pos_vertices(batch, pos, dimen);
  add_region_vertices(batch, texture_region);
  add_color_vertices(batch, graphics.current_color);

  assert_gl_no_err();

  batch->count++;
}

void set_color(float r, float g, float b, float a) {
  graphics.current_color = color01(r, g, b, a);
}
