#include "graphics.h"

struct Mat4 mat4_ortho(float l, float r, float b, float t, float n, float f) {
  struct Mat4 result;
  result.f11 = 2.0f / (r - l);
  result.f12 = 0.0f;
  result.f13 = 0.0f;
  result.f14 = 0.0f;
  result.f21 = 0.0f;
  result.f22 = 2.0f / (t - b);
  result.f23 = 0.0f;
  result.f24 = 0.0f;
  result.f31 = 0.0f;
  result.f32 = 0.0f;
  result.f33 = -2.0f / (f - n);
  result.f34 = 0.0f;
  result.f41 = -((r + l) / (r - l));
  result.f42 = -((t + b) / (t - b));
  result.f43 = -((f + n) / (f - n));
  result.f44 = 1.0f;
  return result;
}

inline float *mat4_addr(struct Mat4 *m) { return (float *)m; }
