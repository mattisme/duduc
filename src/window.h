#ifndef DUDU_WINDOW_H
#define DUDU_WINDOW_H

#define DEFAULT_WINDOW_WIDTH 640
#define DEFAULT_WINDOW_HEIGHT 360
#define DEFAULT_WINDOW_TITLE "dudu"

int window_init(int w, int h, char *title, int swap_interval);
void *_window_get();
int window_update_or_close();
double window_get_time();

int input_init();

#endif
