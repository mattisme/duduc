#ifndef DUDU_GRAPHICS_H
#define DUDU_GRAPHICS_H

#include <dudu.h>

enum ShaderLocation {
  LOC_POS,
  LOC_REGION,
  LOC_COLOR,
  LOC_DIFF,
  LOC_TEX,
  LOC_VIEW,
  SHADER_LOCATION_LEN,
};

struct Shader {
  unsigned int id;
  int locations[SHADER_LOCATION_LEN];
};

struct Mat4 {
  union {
    struct {
      float f11, f12, f13, f14, f21, f22, f23, f24, f31, f32, f33, f34, f41,
          f42, f43, f44;
    };
    float arr[16];
  };
};

struct Mat4 mat4_ortho(float l, float r, float b, float t, float n, float f);
float *mat4_addr(struct Mat4 *m);

void assert_gl_no_err();
int graphics_init(int w, int h);
void graphics_flush();

#endif
