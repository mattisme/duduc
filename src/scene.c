#include "scene.h"
#include <dudu.h>
#include <flecs.h>
#include <stdio.h>

ecs_world_t *world = NULL;
ecs_entity_t draw_system;

int scene_init() {
  world = ecs_init();
  if (!world) {
    fprintf(stderr, "Failed to initialize flecs world.\n");
    return -1;
  }

  ECS_TAG(world, Inactive);

  ECS_COMPONENT(world, Transform);
  ECS_SYSTEM(world, transform_on_add, EcsOnAdd, Transform);

  ECS_COMPONENT(world, Renderer);
  ECS_SYSTEM(world, renderer_on_add, EcsOnAdd, Renderer);

  ECS_TYPE(world, Sprite, Transform, Renderer);
  ECS_SYSTEM(world, renderer_draw, EcsManual, Sprite, !Inactive);
  draw_system = renderer_draw;

  return 0;
}

int scene_update(float delta_time) { return ecs_progress(world, delta_time); }
void scene_draw() { ecs_run(world, draw_system, 0, NULL); }

DuduEntity _scene_new_entity(const char *id_string, const char *type_string) {
  ecs_entity_t id = ecs_new_entity(world, id_string, type_string);
  ECS_TYPE_VAR(id) = ecs_type_from_entity(world, id);
  return ecs_type(id);
}

DuduEntity _scene_new_entities(unsigned int count, const char *type_string) {
  // Obtain the ecs_type_t from the dictionary using the type_string.
}
