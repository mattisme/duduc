#ifndef DUDU_SCENE
#define DUDU_SCENE

#include <flecs.h>

int scene_init();
int scene_update(float delta_time);
void scene_draw();

void transform_on_add(ecs_rows_t *rows);
void renderer_on_add(ecs_rows_t *rows);
void renderer_draw(ecs_rows_t *rows);

#endif
